﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class CharacterOnTile : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip sound;
    public CharacterAttributes characterAttributes;
    public bool HasExited = false;
    public bool isInstantiationDone;

    private Coroutine testRoutine;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(HandleSoundsForCharacter());
    }

    

    IEnumerator PlaySoundFor(float volume, float time)
    {
        audioSource.PlayOneShot(sound, volume);
        yield return new WaitForSeconds(time);
        audioSource.Stop();
    }
    

    IEnumerator HandleSoundsForCharacter()
    {
        HasExited = false;
        yield return new WaitUntil(() => isInstantiationDone);
        float time = 0.5f, volume = 0.7f;
        while (true)
        {
            if(!HasExited)
            {
                //TODO need a standing still value
                switch (characterAttributes.speed)
                {
                    case (int)GameManager.CharacterSpeed.Sneaking:
                        yield return PlaySoundFor(0.7f, time);
                        break;

                    case (int)GameManager.CharacterSpeed.Walking:
                        yield return PlaySoundFor(0.7f, time);
                        break;

                    case (int)GameManager.CharacterSpeed.Running:
                        yield return PlaySoundFor(0.7f, time);
                        break;
                }
            }
            else
            {
                yield return new WaitForEndOfFrame();
            }
        }
        
    }

    void OnTriggerEnter()
    {
        HasExited = false;
    }

    void OnTriggerExit()
    {
        
        HasExited = true;
        //TODO has exited and stop might not matter if destroy is invoked... possibly delete
        audioSource.Stop();
        //Destroy(this);
    }


}

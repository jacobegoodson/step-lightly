﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Security.AccessControl;


public class TileSound : MonoBehaviour
{
    public static AudioClip[] surfaces = new AudioClip[3];
    private bool firstInit;
    public int surface = 0;

    private AudioSource source;

    private bool characterIsMovingOnSurface;

    public enum Surface
    {
        Stone,
        Water,
        Glass
    }

    private GameManager gameManager;

    private List<CharacterOnTile> _objectsOnTheTile = new List<CharacterOnTile>();

    void Start()
    {
        source = GetComponent<AudioSource>();

        //TODO this should only be done once since these are static
        if (!firstInit)
        {
            surfaces[(int)Surface.Water] = Resources.Load("Footsteps_Water") as AudioClip;
            surfaces[(int)Surface.Glass] = Resources.Load("Footsteps_Glass") as AudioClip;

            firstInit = !firstInit;
        }
        
        
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        switch (tag)
        {
            case "Glass":
                surface = (int)Surface.Glass;
                break;
            case "Water":
                surface = (int)Surface.Water;
                break;
            case "Stone":
                surface = (int)Surface.Stone;
                break;
        }
    }

    void CreateSoundHandlerForCharacter(GameObject obj,AudioClip sound)
    {
        CharacterOnTile charOnTile = obj.AddComponent<CharacterOnTile>();
        charOnTile.sound = sound;
        charOnTile.characterAttributes = obj.GetComponent<CharacterAttributes>();

        charOnTile.isInstantiationDone = true;
        
    }

    private AudioClip GetSoundOfTile()
    {
        return surfaces[surface];
    }

    /*void OnTriggerEnter(Collider col)
    {
        CharacterOnTile found = col.gameObject.GetComponent<CharacterOnTile>();
        if (col is CapsuleCollider || col is BoxCollider)
        {
            if (found)
            {
                found.sound = GetSoundOfTile();
            }
            else
            {
                CreateSoundHandlerForCharacter(col.gameObject, GetSoundOfTile());
                /*
                if (surface == (int)Surface.Water)
                {
                    CreateSoundHandlerForCharacter(col.gameObject, );
                }
                else if (surface == (int)Surface.Glass)
                {
                    CreateSoundHandlerForCharacter(col.gameObject, glass);
                }
                else if (surface == (int)Surface.Stone)
                {
                    CreateSoundHandlerForCharacter(col.gameObject, stone);
                }
                
            }
            
            
            //GameObject character = col.gameObject;
            //CharacterAttributes characterAttributes = col.GetComponentInParent<CharacterAttributes>();
           
            //TODO take out box collider when testing is done

        }*/

    void OnTriggerEnter()
    {
        StopAllCoroutines();
        StartCoroutine(PlaySounds());

    }

    IEnumerator PlaySounds()
    {
        while (true)
        {
            //get character velocity here
            //placeholder
            float tvelocity = 0.1f;
            if (tvelocity > 0)
            {

                source.Play();
            } 
        }
    }

    IEnumerator CheckCharacterMovingOnSurface()
    {
        while (true)
        {
            //get character velocity here
            //placeholder
            float tvelocity = 0.1f;
            if (tvelocity > 0.1)
            {
                
            }
        }
    }
}

    

    

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndMenu : MonoBehaviour
{

    public Canvas quitMenu;

    public Button restartText;
    public Button returnText;
    public Button exitText;

    public AudioClip click;
    public AudioClip endYell;
    public AudioClip theme;
    AudioSource audio;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.PlayOneShot(endYell);
        audio.PlayOneShot(theme);

        quitMenu = quitMenu.GetComponent<Canvas>();
        quitMenu.enabled = false;

        restartText = restartText.GetComponent<Button>();
        returnText = returnText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();

    }

    public void ExitPress()
    {

        quitMenu.enabled = true;

        restartText.enabled = false;
        returnText.enabled = false;
        exitText.enabled = false;

    }

    public void returnPress()
    {

        quitMenu.enabled = false;
        Application.LoadLevel("MainMenu");

    }

    public void NoPress()
    {
        quitMenu.enabled = false;

        restartText.enabled = true;
        returnText.enabled = true;
        exitText.enabled = true;

    }

    public void StartLevel()
    {
        
        Application.LoadLevel("Start");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void PlayOnClick()
    {
        audio.PlayOneShot(click);
    }
}

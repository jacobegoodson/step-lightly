﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.W))
        {
	        transform.Translate(Vector3.forward * Time.fixedDeltaTime);
	    }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.fixedDeltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            
        }

    }
}

﻿using UnityEngine;
using System.Collections;

public class BreathingScript : MonoBehaviour
{

    public AudioSource audioSource;
    private AudioClip breathing;
    private CharacterAttributes playerAttributes;
    private bool isBreathingHeavy;

    private Coroutine routine;

    // Use this for initialization
    void Start()
    {
        breathing = Resources.Load("Breathing_Fast") as AudioClip;
        playerAttributes = GetComponentInParent<CharacterAttributes>();
        StartCoroutine(HandleBreathing());
    }

    void Update()
    {
        Debug.Log(playerAttributes.speed);
    }

    IEnumerator HandleBreathing()
    {
        while (true)
        {
            if (playerAttributes.speed == 200) //&& playerAttributes.checkMoving)
            {
                isBreathingHeavy = true;
                if (routine == null)
                {
                    routine = StartCoroutine(Breathing());
                }
            }
            else
            {
                isBreathingHeavy = false;
                audioSource.Stop();
                if (routine != null)
                {
                    StopCoroutine(routine);
                    routine = null;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator Breathing()
    {
        while (true)
        {
            while (isBreathingHeavy)
            {
                audioSource.PlayOneShot(breathing);
                yield return new WaitForSeconds(2.909f);
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
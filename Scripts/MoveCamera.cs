﻿// MODIFICATIONS - Step Lightly for CIST2741
// ==========================================================================================
// Date         Name                Description 
// ==========================================================================================
// 11/21/16     Bill B              Created the Splash screens.  The different screens are placed
                                 // on the scene. The camera goes to each one, pauses and goes to 
                                 // the next screen.  When all acreens are seen, the scene ends.

// 11/22/16     Bill B              Fixed the camers not landing on an object in the same way. 
                                 // Each time the scene was run, the camera would hit the next
                                 // object in a different position. 

//
                                 // 
                                 //

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveCamera : MonoBehaviour {

    public float MyWait = 5;                  // How long to pause over object
    public float speed = 5f;                  // How fast to move the camera
    public float yPos = 0.0f;                  // How far from object to move the camera
    public List<GameObject> Objects;          // List of each object for the camera to go to

    public AudioClip SplashMusic;             // Sound for Splash Screen viewing
    private AudioSource SplashAudioSource;    // Where Splash Screen sound is coming from


    // Use this for initialization
    void Start()
    {
        SplashAudioSource = gameObject.AddComponent<AudioSource>();
        SplashAudioSource.PlayOneShot(SplashMusic);
        StartCoroutine(MoveToObject(0));
    }

    IEnumerator MoveToObject(int iteratingObject)
    {
         
        //Wait for however many seconds
        yield return new WaitForSeconds(MyWait/2);
        bool atDestination = false;

        //Move the camera until at destination
        while (!atDestination)
        {
            yield return new WaitForFixedUpdate();

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Objects[iteratingObject].transform.position.x, Objects[iteratingObject].transform.position.y + yPos, Objects[iteratingObject].transform.position.z), Time.deltaTime * speed);

            if (transform.position.x == Objects[iteratingObject].transform.position.x)   // if the X values match set atDestination == true
            {
                yield return new WaitForSeconds(MyWait);
                atDestination = true;
            }
        }
        //Continue iterating until moved over all objects in list
        if (iteratingObject != Objects.Count - 1)                          //if not at last object then goto the next
            StartCoroutine(MoveToObject(iteratingObject + 1));
        else 
        {    // Add the call to the menu here
            yield return new WaitForSeconds(MyWait);
            Application.LoadLevel("MainMenu");                   // remove this
        }
    }
}


﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class EnemyAppearance : MonoBehaviour {

    private GameObject[] enemy = new GameObject[2];
    private float distance1;
    private float distance2;
    private float lightrange;
    private UnityAction<int> respondToTwin1Walking;
    private UnityAction<int> respondToTwin2Walking;
    private float glassdistance;
    private float waterdistance;
    private bool twin1Heard;
    private bool twin2Heard;

    void Start()
    {
        respondToTwin1Walking = new UnityAction<int>(FlashTwin1);
        respondToTwin2Walking = new UnityAction<int>(FlashTwin2);
        EventManager.StartListening("Twin1SteppingOnHazard", respondToTwin1Walking);
        EventManager.StartListening("Twin2SteppingOnHazard", respondToTwin2Walking);
        enemy = GameObject.FindGameObjectsWithTag("EnemyIcon");
        lightrange = 3f;
        glassdistance = lightrange * 1000;
        waterdistance = lightrange * 1000;
}

    

    void FlashTwin1(int surface)
    {
        if (!twin1Heard)
        {
            switch (surface)
            {
                case (int)HazardLogic.Hazard.Glass:
                    //Debug.Log("Glass1");
                    if (distance1 < glassdistance)
                    {
                        StartCoroutine(twin1HeardAction());
                    }
                    break;
                case (int)HazardLogic.Hazard.Water:
                    //Debug.Log("Water1");
                    if (distance1 < waterdistance)
                    {
                        StartCoroutine(twin1HeardAction());
                    }
                    break;
            }
        }
    }

    void FlashTwin2(int surface)
    {
        if (!twin2Heard)
        {
            switch (surface)
            {
                case (int)HazardLogic.Hazard.Glass:
                    //Debug.Log("Glass1");
                    if (distance1 < glassdistance)
                    {
                        StartCoroutine(twin2HeardAction());
                    }
                    break;
                case (int)HazardLogic.Hazard.Water:
                    //Debug.Log("Water1");
                    if (distance1 < waterdistance)
                    {
                        StartCoroutine(twin2HeardAction());
                    }
                    break;
            }
        }
    }

    void Update()
    {
        distance1 = (enemy[0].transform.position - transform.position).magnitude;
        distance2 = (enemy[1].transform.position - transform.position).magnitude;
        if (!twin1Heard)
        {
            if (distance1 < lightrange)
            {
                enemy[0].GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                enemy[0].GetComponent<MeshRenderer>().enabled = false;
            }
        }

        if (!twin2Heard)
        {
            if (distance2 < lightrange)
            {
                enemy[1].GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                enemy[1].GetComponent<MeshRenderer>().enabled = false;
            }
        } 
    }

    private float displayTime = 5.0f;
    IEnumerator twin1HeardAction()
    {
        twin1Heard = true;
        enemy[0].GetComponent<MeshRenderer>().enabled = true;
        yield return new WaitForSeconds(displayTime);
        twin1Heard = false;
    }
    IEnumerator twin2HeardAction()
    {
        twin2Heard = true;
        enemy[1].GetComponent<MeshRenderer>().enabled = true;
        yield return new WaitForSeconds(displayTime);
        twin2Heard = false;
    }
}

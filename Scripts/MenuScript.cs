﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuScript : MonoBehaviour {

    public Canvas quitMenu;
    public Canvas volumeMenu;

    public Button startText;
    public Button volumeText;
    public Button exitText;

    public AudioClip click;
    public AudioClip theme;
    AudioSource audio;

    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.PlayOneShot(theme);

        quitMenu = quitMenu.GetComponent<Canvas>();
        quitMenu.enabled = false;
        volumeMenu = volumeMenu.GetComponent<Canvas>();

        startText = startText.GetComponent<Button>();
        volumeText = volumeText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();

        
        volumeMenu.enabled = false;
    }

    public void ExitPress()
    {

        quitMenu.enabled = true;
        volumeMenu.enabled = false;

        startText.enabled = false;
        volumeText.enabled = false;
        exitText.enabled = false;
        
    }

    public void VolumePress()
    {

        quitMenu.enabled = false;
        volumeMenu.enabled = true;

        startText.enabled = false;
        volumeText.enabled = false;
        exitText.enabled = false;

    }

    public void NoPress()
    {
        quitMenu.enabled = false;
        volumeMenu.enabled = false;

        startText.enabled = true;
        volumeText.enabled = true;
        exitText.enabled = true;

    }

    public void StartLevel()
    {
        Application.LoadLevel("Start");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void PlayOnClick()
    {
        audio.PlayOneShot(click);
    }
}

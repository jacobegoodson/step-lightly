﻿using UnityEngine;
using System.Collections;

public class CharacterAttributes : MonoBehaviour {

    public int speed;
    public bool checkMoving;
    public string _tag;

    void Start()
    {
        speed = 100;
    }

    void Update()
    {
        if (tag == "Player")
        {
            checkMoving = GetComponentInParent<PlayerController>().moving;
        }
        speed = 100;
        if (checkMoving)
        {
            if (Input.GetKey("space"))
            {
                speed = (speed / 2);
            }
            if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = (speed * 2);
            }
        }
    }
}

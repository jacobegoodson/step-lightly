﻿using UnityEngine;
using System.Collections;


public class WalkingSounds : MonoBehaviour
{
    // BillB Added --------------------------
    private AudioClip LftFoot;              // Sound for Left foot
    private AudioClip RtFoot;               // Sound for Right foot
    private AudioClip Glass;                // Sound for walking on Glass
    private AudioClip Water;                // Sound for walking on Water
    public float volume = 0.25f;           // Volume for music
    public bool OnGlass = false;           // Flag for when player on Glass made public so we can test while playing
    public bool OnWater = false;           // Flag for when player on Water made public so we can test while playing

    private AudioSource audioSource;
    //public AudioSource LftAudioSource;    // Where Left foot sound is coming from
    //private AudioSource RtAudioSource;     // Where Right foot sound is coming from
    //private AudioSource glassAudioSource;  // Where walking on glass is coming from
    //private AudioSource waterAudioSource;  // Where walking on water is coming from
    private bool LftStp = true;            // Flag for the left foot.  When true Play Left foot sound, else plat right foot sound
    private int VoiceCnt = 0;              // Counts the times you use the MomVoice
    private bool firstInit;
    // End BillB Added ----------------------
    

    private Rigidbody _rigidbody;

    // Use this for initialization
    void Start ()
    {
        _rigidbody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        if (!firstInit)
        {
            Water = Resources.Load("Test 3 (Water)") as AudioClip;
            Glass = Resources.Load("Test 4 (Glass)") as AudioClip;
            RtFoot = Resources.Load("footstep_right") as AudioClip;
            LftFoot = Resources.Load("footstep_left") as AudioClip;

            firstInit = !firstInit;
        }

        
        //gameAudioSource.volume = volume;    // Set the volume
        //gameAudioSource.loop = true;        // set the Game Music always loop when playing
        //gameAudioSource.PlayOneShot(Game);  // Play the Game music  
        StartCoroutine(PlayFootSteps());

        // End BillB Added ----------------------

    }

    bool IsMoving()
    {
        return _rigidbody.velocity.sqrMagnitude > 0;
    }

    

    // BillB Added --------------------------
    IEnumerator PlayFootSteps()
    {
        float waitTime = 0.3f;
        while (true)
        {
            while (IsMoving())
            {
                
                if (OnGlass)                               // if on glass, play glass sound
                {
                    audioSource.PlayOneShot(Glass);
                    yield return new WaitForSeconds(waitTime);
                }
                else if (OnWater)                          // if on water, play water sound
                {
                    audioSource.PlayOneShot(Water);
                    yield return new WaitForSeconds(waitTime);
                }

                else                                       // elst play normal footsteps sound
                {                                          // Alternate steps, play left, play right, play left....  etc
                    if (LftStp)  
                    {
                        //always emit a sound so that the ai can "hear" even when stepping on stone
                        EventManager.TriggerEvent("PlayerSteppingOnHazard", 5);
                        audioSource.PlayOneShot(LftFoot);
                        LftStp = false;
                        yield return new WaitForSeconds(waitTime);
                    }
                    else
                    {
                        EventManager.TriggerEvent("PlayerSteppingOnHazard", 5);
                        LftStp = true;
                        audioSource.PlayOneShot(RtFoot);
                        yield return new WaitForSeconds(waitTime);
                    }
                    
                }
            }
            
            yield return null;
        }
    }

    
}

// BillB Added --------------------------
// End BillB Added ----------------------






﻿using UnityEngine;
using System.Collections;

public class WalkToPlayer : MonoBehaviour
{
    public GameObject ObjectToWalkTo;
    private NavMeshPath path = new NavMeshPath();
    //the speed at which a twin can move!
    public float moveSpeed = 0.05f;
    private Coroutine stillRunning; 

	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(StartPathFinding());
	}
	
	// Update is called once per frame
	void Update ()
	{
	    //transform. //Translate(path.corners[0]);
        
	    //transform.position = Vector3.MoveTowards(transform.position, path.corners[1], 0.1f);
        
        //
        //
        //InternalNavMeshAgent.CalculatePath(target.transform.position, InternalNavMeshAgent.path);
        //show the path as it is being drawn... if need be
        ShowPath();
	}

    IEnumerator StartPathFinding()
    {
        yield return new WaitForSeconds(1);
        //InternalNavMeshAgent.CalculatePath(target.transform.position, path);
        NavMesh.CalculatePath(transform.position ,ObjectToWalkTo.transform.position, NavMesh.AllAreas, path);
        yield return new WaitForSeconds(1);
        stillRunning = StartCoroutine(FollowPath());
    }

    IEnumerator FollowPath()
    {
        foreach (Vector3 pathCorner in path.corners)
        {
            Vector3 pathCornerCopy = pathCorner;
            pathCornerCopy.y = 0.0f;
            //transform.Rotate(pathCornerCopy);
            while (Vector3.Distance(transform.position, pathCornerCopy) > 0.05)
            {
                Debug.Log(Vector3.Distance(transform.position, pathCornerCopy));
                transform.position = Vector3.MoveTowards(transform.position, pathCornerCopy, 0.01f);
                //transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, pathCorner, (float)1.0 * Time.deltaTime, 0.0f));
                Quaternion rot = Quaternion.LookRotation(pathCorner - transform.position);
                rot.x = 0.0f;
                rot.z = 0.0f;
                transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime*2.0f);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    void ShowPath()
    {
        Vector3 prev = transform.position;
        foreach (Vector3 nextCorner in path.corners)
        {
            Debug.DrawLine(prev, nextCorner, Color.red);
            prev = nextCorner;
        }
    }

}

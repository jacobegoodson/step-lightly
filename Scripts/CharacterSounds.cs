﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class CharacterSounds : MonoBehaviour {

    int finalSound = 0;

    AudioClip water, glass, stone;
    AudioSource audioSource;

    enum Surface
    {
        Stone, Glass, Water
    }

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();

       water =  Resources.Load("Footsteps_Water") as AudioClip;
       glass =  Resources.Load("Footsteps_Glass") as AudioClip;
       //stone =  Resources.Load("Footsteps_Stone") as AudioClip;

        StartCoroutine(MakeSound());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider col)
    {
        

        if (col.GetComponentInParent<Water>())
        {
            finalSound = 2;
            Debug.Log("im on water");
        }
        else if (col.GetComponentInParent<Glass>())
        {
            finalSound = 1;
            Debug.Log("im on glass");
        }
        else if (col.GetComponentInParent<Stone>())
        {
            finalSound = 0;
            Debug.Log("im on stone");
        }




    }

    IEnumerator MakeSound() {
        while (true)
        {
            Debug.Log(finalSound);

            switch (finalSound)
            {
                case (int) Surface.Stone:
                    audioSource.PlayOneShot(stone, 0.7f);
                    break;

                case (int)Surface.Water:
                    audioSource.PlayOneShot(water, 0.7f);
                    yield return new WaitForSeconds(2);
                    break;

                case (int)Surface.Glass:
                    audioSource.PlayOneShot(glass, 0.7f);
                    break;
            }

            
            yield return new WaitForEndOfFrame();
        }
    }

}

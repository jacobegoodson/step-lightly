﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public GameObject[] spawnPoints = new GameObject[4];

    public GameObject player;
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject winpoint;

    void Start()
    {
        reshuffle(spawnPoints);
        SpawnPlayer();
        SpawnEnemy();
        SpawnExit();
    }

    void reshuffle(GameObject[] spawnPoints)
    { 
        for (int t = 0; t<spawnPoints.Length; t++)
        {
            GameObject tmp = spawnPoints[t];
            int r = Random.Range(t, spawnPoints.Length);
            spawnPoints[t] = spawnPoints[r];
            spawnPoints[r] = tmp;
        }
    }

    void SpawnPlayer()
    {
        Instantiate(player, spawnPoints[0].transform.position, Quaternion.identity);
    }
    void SpawnEnemy()
    {
        Instantiate(enemy1, spawnPoints[1].transform.position, Quaternion.identity);
        Instantiate(enemy2, spawnPoints[2].transform.position, Quaternion.identity);
    }
    void SpawnExit()
    {
        Instantiate(winpoint, spawnPoints[3].transform.position, Quaternion.identity);
    }

    void TestSetup()
    {
        
    }
}

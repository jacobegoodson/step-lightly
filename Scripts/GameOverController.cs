﻿using UnityEngine;
using System.Collections;

public class GameOverController : MonoBehaviour {

    private Collider enemy;
    private Collider exit;

    // Use this for initialization
    void Start ()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Collider>();
        exit = GameObject.FindGameObjectWithTag("Exit").GetComponent<Collider>();
    }
	

    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider == exit)
        {
            GetComponent<MouseLook>().lockCursor = false;
            Application.LoadLevel("WinMenu");
        }
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.GetComponentInParent<Transform>().tag == "Exit")
        {
            GetComponent<MouseLook>().lockCursor = false;
            Application.LoadLevel("WinMenu");
        }
        if (col.GetComponent<Transform>().tag == "Enemy")
        {
            if (!col.GetComponent<TwinAI>().isCrying)
            {
                GetComponent<MouseLook>().lockCursor = false;
                Application.LoadLevel("EndMenu");
            }
        }
    }
}

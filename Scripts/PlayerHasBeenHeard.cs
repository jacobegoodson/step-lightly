﻿using UnityEngine;
using System.Collections;

public class PlayerHasBeenHeard : MonoBehaviour
{
    private TwinAI Twin1;
    private TwinAI Twin2;
    private SpriteRenderer exclamation;
    void Start()
    {
        GameObject[] twins = GameObject.FindGameObjectsWithTag("Enemy");
        Twin1 = twins[0].GetComponent<TwinAI>();
        Twin2 = twins[1].GetComponent<TwinAI>();
        exclamation = GameObject.FindGameObjectWithTag("exclamation").GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (Twin1.alarmed || Twin2.alarmed)
        {
            exclamation.enabled = true;
        }
        else
        {
            exclamation.enabled = false;
        }
    }
        


   
}

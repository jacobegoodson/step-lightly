﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class TwinAI : MonoBehaviour {

    //IDLE is a test state
    private enum State { Patrol, Pursuit, Investigation, IDLE, Cower};
    //way points to patrol, takes up to four of them and patrols them in repeating order
    public GameObject[] PatrolWayPoints = new GameObject[4];
    //a location to investigate
    private Vector3 _investigationLocation;
    private NavMeshPath _path = new NavMeshPath();
    //the speed at which a twin can move!
    private float MoveSpeed = 2f;
    //private IEnumerator CurrentStateActions;
    private State _currentState = State.Patrol;
    private bool _isSwitchTime = true;
    //keeps track of all coroutines that have been called, including nested ones
    private List<IEnumerator> _runningRoutines = new List<IEnumerator>();
    // a reference to the player, found by the player tag
    private GameObject _player;
    //keeps track of how long a player has not been visible
    private int _playerVisibleCounter = 0;
    // Use this for initialization

    private AudioSource audioSource;
    private AudioClip crying;

    private CharacterAttributes attributes;
    private CharacterAttributes playerAttributes;
    
    private UnityAction<int> InvestigateEventAction;
    private UnityAction<int> CowerAction;

    private Rigidbody _rigidbody;
    private bool _firstInit;
    public bool isCrying;
    void Start()
    {
        //randomize the waypoints
        reshuffle(PatrolWayPoints);
        audioSource = GetComponent<AudioSource>();
        if (!_firstInit)
        {
            crying = Resources.Load("crying") as AudioClip;
            _firstInit = !_firstInit;
        }
        attributes = GetComponent<CharacterAttributes>();
        _rigidbody = GetComponent<Rigidbody>();
        //start Listening for player stepping on surfaces
        InvestigateEventAction = new UnityAction<int>(RespondToSounds);
        EventManager.StartListening("PlayerSteppingOnHazard", InvestigateEventAction);
        //start listening for mothers voice
        CowerAction = new UnityAction<int>(RespondToMothersVoice);
        EventManager.StartListening("Mother's Voice", CowerAction);

        _player = GameObject.FindGameObjectWithTag("Player");
        playerAttributes = _player.GetComponent<CharacterAttributes>();
        StartCoroutine(StartTwin());
    }

    void reshuffle(GameObject[] spawnPoints)
    {
        for (int t = 0; t < spawnPoints.Length; t++)
        {
            GameObject tmp = spawnPoints[t];
            int r = Random.Range(t, spawnPoints.Length);
            spawnPoints[t] = spawnPoints[r];
            spawnPoints[r] = tmp;
        }
    }

    IEnumerator StartTwin()
    {
        yield return new WaitForSeconds(1);

        StartCoroutine(LookForPlayer());
        StartCoroutine(TwinMind());
    }

    public bool alarmed;
    IEnumerator TwinMind()
    {
        // wait until the bot has a state in the first place
        // yield return new WaitUntil(() => null != CurrentStateActions);
        // every three frames test to see if the coroutine needs to be changed!
        while (true)
        {
            if (_isSwitchTime)
            {
                _isSwitchTime = false;
                switch (_currentState)
                {
                    case State.Patrol:
                        alarmed = false;
                        StopRoutines();
                        StartRoutine(Patrol());
                        break;
                    case State.Investigation:
                        alarmed = true;
                        StopRoutines();
                        var temp = _investigationLocation;
                        temp.y = 0.0f;
                        StartRoutine(Investigate(temp));
                        break;
                    case State.Pursuit:
                        alarmed = true;
                        StopRoutines();
                        StartRoutine(Persue());
                        break;
                    case State.Cower:
                        alarmed = false;
                        StopRoutines();
                        StartRoutine(Cower());
                        break;
                }
            }
            //wait three frames
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
           // switchState(State.Investigation); //optional arguement to investigation
        }
    }
    

    //patrols the outer waypoints
    public IEnumerator Patrol()
    {
        while (true)
        {
            foreach (var patrolWayPoint in PatrolWayPoints)
            {
                NavMesh.CalculatePath(transform.position, patrolWayPoint.transform.position, NavMesh.AllAreas, _path);
                yield return StartRoutine(FollowPath(MoveSpeed));
            }
        }
    }

    private IEnumerator LookAround(Quaternion rot)
    {
        _rigidbody.velocity = Vector3.zero;
        var counter = 0;
        while (counter < 20)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 10.0f);
            counter++;
            yield return new WaitForEndOfFrame();
        }
        counter = 0;
        while (counter < 10)
        {
            transform.Translate(Vector3.forward * Time.deltaTime);
            counter++;
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(1);
        _rigidbody.velocity = Vector3.up;
    }

    //investigates a disturbance!
    IEnumerator Wait2Secs()
    {
        yield return new WaitForSeconds(2f);
        alarmed = false;
    }
    private IEnumerator Investigate(Vector3 disturbanceLocation)
    {
        StartCoroutine(Wait2Secs());
        NavMesh.CalculatePath(transform.position, disturbanceLocation, NavMesh.AllAreas, _path);
        yield return StartRoutine(FollowPath(MoveSpeed));
        //once twin has made it to path... look around
        //look around code
        for (int i = 0; i < 2; i++)
        {
            foreach (var patrolWayPoint in PatrolWayPoints)
            {

                Vector3 positionCopy = patrolWayPoint.transform.position;
                positionCopy.y = 0.0f;
                Quaternion rot = Quaternion.LookRotation(positionCopy - transform.position);
                rot.x = 0.0f;
                rot.z = 0.0f;
                yield return StartRoutine(LookAround(rot));
                // return to disturbance position
                Vector3 disturbanceLocationCopy = disturbanceLocation;
                disturbanceLocationCopy.y = 0.0f;
                //transform.rotation = Quaternion.LookRotation(disturbanceLocationCopy - transform.position);
                while (Vector3.Distance(transform.position, disturbanceLocationCopy) > 0.05f)
                {
                    MoveTowardsPosition(disturbanceLocationCopy, MoveSpeed);
                    yield return new WaitForEndOfFrame();
                }

                yield return new WaitForSeconds(2);
            }
        }

        //look around code end

        switchState(State.Patrol);
    }

    private IEnumerator PlayerVisibleCounter()
    {
        while (true)
        {
            if (_playerVisibleCounter != 0)
            {
                _playerVisibleCounter--;
            }
            yield return new WaitForSeconds(1);
        }
    }

    //TODO possibly make AI investigate last known location for a neater looking ending to pursuit

    //LookForPlayer is always running, constantly checks to see if player is in LOS, if so pursue state activated. 
    //if player leaves LOS for five seconds, state changed back to patrol 

    private IEnumerator LookForPlayer()
    {
        if (!isCrying)
        {
            StartCoroutine(PlayerVisibleCounter());
            Vector3 fromAIToPlayer;
            RaycastHit hit;
            float maxAngle = 90, angle;
            //determines whether to check the playerVisibleCounter or not
            bool checkCounter = false;
            maxAngle *= 0.5f;
            while (true)
            {
                fromAIToPlayer = _player.transform.position - transform.position;
                angle = Vector3.Angle(fromAIToPlayer, transform.forward);
                //Debug.DrawRay(transform.position, fromAIToPlayer.normalized);
                if (angle <= maxAngle)
                {
                    if (Physics.Raycast(transform.position, fromAIToPlayer.normalized, out hit, 200))
                    {
                        //player is visible by the AI!
                        if (hit.collider.gameObject == _player)
                        {
                            _playerVisibleCounter = 5;
                            checkCounter = true;
                            if (_currentState != State.Pursuit)
                            {
                                switchState(State.Pursuit);
                            }
                        }
                    }
                }
                if (checkCounter)
                {
                    if (_playerVisibleCounter == 0)
                    {
                        checkCounter = false;
                        switchState(State.Patrol);
                    }
                }
                //wait three frames to check if visible
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
            }
        }
    }

    
    private IEnumerator Persue()
    {
        while (true)
        {
            NavMesh.CalculatePath(transform.position, _player.transform.position, NavMesh.AllAreas, _path);
            //TODO change animation state to a pursue looking one
            //TODO give a different speed for the pursuit
            yield return StartRoutine(FollowPath(MoveSpeed));
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
        }
    }

    private void RespondToMothersVoice(int useless)
    {
        transform.rotation = Quaternion.Euler(transform.rotation.x, -transform.rotation.y, transform.rotation.z);
        ChangeStateCower();
    }

    private IEnumerator Cower()
    {
        isCrying = true;
        float playerDistance = (_player.transform.localPosition - transform.position).magnitude;
        if (playerDistance < 500)
        {
            audioSource.PlayOneShot(crying);
            yield return new WaitForSeconds(7.706f);
        }
        ChangeStatePatrol();
        isCrying = false;
    }

    //helper coroutines for navigation
    private IEnumerator FollowPath(float speed)
    {
        foreach (Vector3 pathCorner in _path.corners)
        {
            Vector3 pathCornerCopy = pathCorner;
            pathCornerCopy.y = 0.0f;
            //transform.Rotate(pathCornerCopy);
            while (Vector3.Distance(transform.position, pathCornerCopy) > 0.05)
            {
                MoveTowardsPosition(pathCornerCopy, speed);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    // slowly lerps both object's position and rotation towards given position
    void MoveTowardsPosition(Vector3 position, float speed)
    {
        _rigidbody.velocity = transform.forward * (1/100f);
        Vector3 positionCopy = position;
        positionCopy.y = 0.0f;
        //Debug.Log(Vector3.Distance(transform.position, positionCopy));
        transform.position = Vector3.MoveTowards(transform.position, positionCopy, speed * 0.01f);
        
        //transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, pathCorner, (float)1.0 * Time.deltaTime, 0.0f));
        Quaternion rot = Quaternion.LookRotation(positionCopy - transform.position);
        rot.x = 0.0f;
        rot.z = 0.0f;
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 10.0f);
    }

    
    //pass routines in by reference...
    private Coroutine StartRoutine(IEnumerator routine)
    {
        _runningRoutines.Add(routine);
        return StartCoroutine(routine);
    }

    private void StopRoutines()
    {
        foreach (var runningRoutine in _runningRoutines)
        {
            StopCoroutine(runningRoutine);
        }
        _runningRoutines.Clear();
    }

    private void switchState(State state)
    {
        _currentState = state;
        _isSwitchTime = true;
    }

    //public API for changing the state of a bot on the fly for other programmers
    //bot will go back to patrolling automatically after any state is done being processed
    public void ChangeStatePatrol()
    {
        switchState(State.Patrol);
    }
    public void ChangeStatePursuit()
    {
        switchState(State.Pursuit);
    }
    public void ChangeStateInvestigation(Vector3 investigationLocation)
    {
        this._investigationLocation = investigationLocation;
        switchState(State.Investigation);
    }

    public void ChangeStateCower()
    {
        switchState(State.Cower);
    }

    void RespondToSounds(int surface)
    {
        if (!isCrying)
        {
            Vector3 playerLocation;
            if (IsCharacterTooLoud(surface, out playerLocation))
            {
                if (_currentState != State.Pursuit && _currentState != State.Cower)
                {
                    ChangeStateInvestigation(playerLocation);
                }
            }
        }
    }
    //data for IsCharacterTooLoud
    private int _stoneDistance = 5;
    private int _waterDistance = 7;
    private int _glassDistance = 8;
    private float _speed;
    bool IsCharacterTooLoud(int surface, out Vector3 playerLocation)
    {
        Vector3 fromEnemyToPlayer = _player.transform.position - transform.position;
        float enemyDistanceFromPlayer = fromEnemyToPlayer.magnitude;

        playerLocation = transform.position + fromEnemyToPlayer;
        _speed = playerAttributes.speed/100f;
        //debugVector.Scale(new Vector3(stoneDistance, stoneDistance, stoneDistance));
        //Debug.DrawLine(playerLocation, playerLocation + -fromEnemyToPlayer.normalized * _stoneDistance * _speed, Color.red);
        
        if (_player.GetComponent<Rigidbody>().velocity.sqrMagnitude > 0)
        {
            if (_speed > 0.9999f)
            {
                switch (surface)
                {
                    case (int)HazardLogic.Hazard.Water:
                        if (enemyDistanceFromPlayer < _waterDistance * _speed)
                        {
                            return true;
                        }
                        return false;

                    case (int)HazardLogic.Hazard.Glass:
                        if (enemyDistanceFromPlayer < _glassDistance * _speed)
                        {
                            return true;
                        }
                        return false;
                    case 5: //character is just walking
                        if (enemyDistanceFromPlayer < _stoneDistance * _speed)
                        {
                            return true;
                        }
                        return false;
                }
            }
        }
        
       
        //satisfy the crappy compiler
        return false;
    }
}


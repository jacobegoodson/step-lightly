﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[System.Serializable]
public class MyEvent : UnityEvent<int>
{
}

public class EventManager : MonoBehaviour
{

    private Dictionary<string, MyEvent> _eventDictionary;

    private static EventManager _eventManager;

    public static EventManager Instance
    {
        get
        {
            if (!_eventManager)
            {
                _eventManager = FindObjectOfType<EventManager>();
                if (!_eventManager)
                {
                    Debug.LogError("There needs to be at least one active EventManager script on a GameObject");
                }
                else
                {
                    _eventManager.Init();
                }
            }
            //Debug.Log("Event Manager Online!");
            return _eventManager;
        }
    }

    private void Init()
    {
        if (_eventDictionary == null)
        {
            _eventDictionary = new Dictionary<string, MyEvent>();
        }
    }
    
    public static void StartListening(string eventName, UnityAction<int> listener)
    {
        MyEvent thisEvent = null;
        if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.AddListener(listener);
        }
        else
        {
            thisEvent = new MyEvent();
            thisEvent.AddListener(listener);
            Instance._eventDictionary.Add(eventName, thisEvent);
        }
    }


    public static void StopListening(string eventName, UnityAction<int> listener)
    {
        if (_eventManager == null)
        {
            return;
        }

        MyEvent thisEvent = null;
        if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, int data)
    {
        MyEvent thisEvent = null;
        if (Instance._eventDictionary.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke(data);
        }
    }
}

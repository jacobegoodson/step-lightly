﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CoroutineHelper : MonoBehaviour
{
    // run more than one coroutine, once one finishes they all finish!
    public bool isRaceOver(List<Coroutine> routines)
    {
        foreach (var coroutine in routines)
        {
            if (coroutine == null)
            {
                foreach (var routine in routines)
                {
                    if (routine != coroutine)
                    {
                        StopCoroutine(routine);
                    }
                }
                return true;
            }
        }
        return false;
    }
}

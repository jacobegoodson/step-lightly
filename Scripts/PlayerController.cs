﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerController : MonoBehaviour
{
    float dt;
    public float turnSpeed = 100.0f;
    private float rotation = 0.0f;
    private Quaternion qTo = Quaternion.identity;
    private CharacterAttributes characterAttributes;
    public bool moving = false;
    private Rigidbody rigidBody;
    private AudioSource momVoice;
    public AudioClip momVoiceClip;
    private int voiceCopyPower = 1;
    private bool voice;
    

    enum Keys
    {
        W,A,S,D
    }

    private List<Keys> keys = new List<Keys>();

    private Vector3 finalVelocity = Vector3.zero;

    // Use this for initialization
    void Start () 
	{
        dt = Time.deltaTime;
	    characterAttributes = GetComponent<CharacterAttributes>();
	    rigidBody = GetComponent<Rigidbody>();
        momVoice = gameObject.AddComponent<AudioSource>();
    }

    private float cutOffVoice = 0.0f;
    private bool triggerOnce;
    void Update()
    {
        if (Input.GetKey("w"))
        {
            moving = true;
            keys.Add(Keys.W);
        }
        if (Input.GetKey("a"))
        {
            moving = true;
            keys.Add(Keys.A);
        }
        if (Input.GetKey("s"))
        {
            moving = true;
            keys.Add(Keys.S);
        }
        if (Input.GetKey("d"))
        {
            moving = true;
            keys.Add(Keys.D);
        }

        if (!Input.anyKey)
        {
            moving = false;
        }

        foreach (var key in keys)
        {
            switch (key)
            {
                case Keys.W:
                    finalVelocity += forwardMove();
                    break;
                case Keys.A:
                    finalVelocity += StrafeLeft();
                    break;
                case Keys.S:
                    finalVelocity += backMove();
                    break;
                case Keys.D:
                    finalVelocity += StrafeRight();
                    break;
            }
        }

        if (Input.GetKey(KeyCode.V) && voiceCopyPower == 1)
        {
            voice = true;
            voiceCopyPower = 0;
        }

        if (voice)
        {
            if (!triggerOnce)
            {
                EventManager.TriggerEvent("Mother's Voice", 5);
                triggerOnce = !triggerOnce;
            }
            
            cutOffVoice += Time.deltaTime;
            if (cutOffVoice < 2.0f)
            {
                momVoice.PlayOneShot(momVoiceClip);
            }
            else
            {
                momVoice.Stop();
            }
            if (cutOffVoice > 3.0f)
            {
                cutOffVoice = 3.0f;
            }
        }

        keys.Clear();
    }


	// Update is called once per frame
	void FixedUpdate ()
	{
	    rigidBody.velocity = finalVelocity;
	    finalVelocity = Vector3.zero;
	}

    Vector3 forwardMove()
    {
        return transform.forward * characterAttributes.speed * Time.deltaTime;
    }
    Vector3 backMove()
    {
        return transform.forward * -characterAttributes.speed * Time.deltaTime;
    }

    Vector3 StrafeLeft()
    {
        return transform.right * -characterAttributes.speed * Time.deltaTime;
    }

    Vector3 StrafeRight()
    {
        return transform.right * characterAttributes.speed * Time.deltaTime;
    }
}





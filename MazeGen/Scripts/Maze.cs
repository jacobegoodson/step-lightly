﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Maze : MonoBehaviour
{
    //uses modified prims algorithm!

    enum Walls { North = 1, South = 4, East = 2, West = 3 };
    // makes inner classes visible in the inspector
    [System.Serializable]

    public class Cell
    {
        public bool visited = false;
        public GameObject north; //1
        public GameObject east;  //2
        public GameObject west;  //4
        public GameObject south; //3
    }

    public GameObject Entrance;
    public GameObject Exit;

    private List<GameObject> wallList = new List<GameObject>(); 

    public GameObject Wall;
    public float wallLength = 1.0f;
    public int xSize = 5;
    public int ySize = 5;
    private Vector3 initialPos;
    private GameObject wallHolder;
    public Cell[] cells;
    public int currentCell = 0;
    private int totalCells;
    private int visitedCells = 0;
    private bool startedBuilding = false;
    private int currentNeighbour = 0;
    private List<int> lastCells;
    private int backingUp = 0;
    private int wallToBreak = 0;

    public GameObject glassHazard;
    public GameObject waterHazard;
    // Use this for initialization
    void Start () {
        // create all the walls needed for the maze as soon as the game starts
	    CreateWalls();
	}

    void CreateWalls()
    {
        wallHolder = new GameObject();
        wallHolder.name = "Maze";
        initialPos = new Vector3((-xSize/2) + wallLength/2, 0.0f, (-ySize/2) + wallLength / 2);

        Quaternion hazardPrefabRotation = glassHazard.transform.rotation;

        Vector3 myPos = initialPos;
        GameObject tempWall = new GameObject();
        //this is for the x axis
        for (int i = 0; i < ySize; i++)
        {
            for (int j = 0; j <= xSize; j++)
            {
                // iterate near the origin, pos determines the position for each section of the wall
                myPos = new Vector3(initialPos.x + (j * wallLength)-wallLength/2.0f,0.0f, initialPos.z+(i*wallLength)-wallLength/2);
                // create a wall at myPos, rotate the wall according the the z axis
                tempWall = Instantiate(Wall, myPos, Quaternion.identity) as GameObject;

                int rand = Random.Range(0,100);
                if (rand < 10)
                {
                    Vector3 copyPos = myPos;
                    copyPos.y = -0.48f;
                    rand = Random.Range(0, 1000);
                    if (rand > 600)
                    {
                        Instantiate(glassHazard, copyPos, hazardPrefabRotation);
                    }
                    else
                    {
                        Instantiate(waterHazard, copyPos, hazardPrefabRotation);
                    }
                }
                

                wallList.Add(tempWall);
                //this will keep them from cluttering the object hierarchy
                tempWall.transform.parent = wallHolder.transform;
            }
        }

        // this is for the y axis
        for (int i = 0; i <= ySize; i++)
        {
            for (int j = 0; j < xSize; j++)
            {
                // iterate near the origin, pos determines the position for each section of the wall
                myPos = new Vector3(initialPos.x + (j * wallLength), 0.0f, initialPos.z + (i * wallLength) - wallLength);
                // create a wall at myPos, rotate the wall according the the z axis
                //Quaternion.Euler(0.0f, 90.0f, 0.0f);
                tempWall = Instantiate(Wall, myPos, Quaternion.LookRotation(Vector3.right)) as GameObject;
                wallList.Add(tempWall);
                tempWall.transform.parent = wallHolder.transform;
                
            }
        }

        //walls are finished, create the cells
        CreateCells();
    }

    void CreateCells()
    {
        lastCells = new List<int>();
        lastCells.Clear();
        totalCells = xSize * ySize;
        int children = wallHolder.transform.childCount;
        GameObject[] walls = new GameObject[children];
        cells = new Cell[xSize*ySize];
        int eastWestProcess = 0;
        int childprocess = 0;
        int termCount = 0;

        //Gets all the children
        for (int i = 0; i < children; i++)
        {
            walls[i] = wallHolder.transform.GetChild(i).gameObject;
        }

        for (int cellprocess = 0; cellprocess < cells.Length; cellprocess++)
        {
            cells[cellprocess] = new Cell();
            cells[cellprocess].east = walls[eastWestProcess];
            cells[cellprocess].south = walls[childprocess+(xSize+1)*ySize];
            if (termCount == xSize)
            {
                eastWestProcess += 2;
                termCount = 0;
            }
            else
                eastWestProcess++;

            termCount++;
            childprocess++;
            cells[cellprocess].west = walls[eastWestProcess];
            cells[cellprocess].north = walls[(childprocess + (xSize + 1) * ySize) + xSize - 1];

        }
        //GiveMeNeighbour();
        CreateMaze();
    }

    void CreateMaze()
    {
        while (visitedCells < totalCells)
        {
            if (startedBuilding)
            {
                GiveMeNeighbour();
                if (cells[currentNeighbour].visited == false && cells[currentCell].visited)
                {
                    BreakWall();
                    cells[currentNeighbour].visited = true;
                    visitedCells++;
                    lastCells.Add(currentCell);
                    currentCell = currentNeighbour;
                    if (lastCells.Count > 0)
                    {
                        backingUp = lastCells.Count - 1;
                    }
                }
            }
            else
            {
                currentCell = Random.Range(0, totalCells);
                cells[currentCell].visited = true;
                visitedCells++;
                startedBuilding = true;
            }
            
        }
        
        EnterAndExit();
    }

    private void BreakWall()
    {
        switch (wallToBreak)
        {
            case (int)Walls.North: Destroy(cells[currentCell].north); break;        
            case (int)Walls.East:  Destroy(cells[currentCell].east);  break;
            case (int)Walls.South: Destroy(cells[currentCell].south); break;
            case (int)Walls.West:  Destroy(cells[currentCell].west);  break;    
        }
    }

    void GiveMeNeighbour()
    {
        int length = 0;
        int[] neighbours = new int[4];
        int[] connectionWall = new int[4];
        int check = 0;
        //TODO fix this crap VVV
        check = ((currentCell + 1)/xSize);
        check -= 1;
        check *= xSize;
        check += xSize;

        
        if (currentCell + 1 < totalCells && (currentCell + 1) != check)
        {
            if (cells[currentCell + 1].visited == false)
            {
                neighbours[length] = currentCell + 1;
                connectionWall[length] = (int) Walls.West;
                length++;
            }
        }

        if (currentCell - 1 >= 0 && currentCell != check)
        {
            if (cells[currentCell - 1].visited == false)
            {
                neighbours[length] = currentCell - 1;
                connectionWall[length] = (int) Walls.East;
                length++;
            }
        }

        if (currentCell + xSize < totalCells)
        {
            if (cells[currentCell + xSize].visited == false)
            {
                neighbours[length] = currentCell + xSize;
                connectionWall[length] = (int) Walls.North;
                length++;
            }
        }

        if (currentCell - xSize >= 0)
        {
            if (cells[currentCell - xSize].visited == false)
            {
                neighbours[length] = currentCell - xSize;
                connectionWall[length] = (int) Walls.South;
                length++;
            }
        }

        if (length != 0)
        {
            int theChosenOne = Random.Range(0, length);
            currentNeighbour = neighbours[theChosenOne];
            wallToBreak = connectionWall[theChosenOne];
        }
        else
        {
            if (backingUp > 0)
            {
                currentCell = lastCells[backingUp];
                backingUp--;
            }
        }
    }

    void EnterAndExit()
    {
        foreach (GameObject o in wallList)
        {
            //Debug.Log((int)o.transform.localPosition.x + " " + (int)o.transform.localPosition.z);
            if ((int)o.transform.localPosition.x == -6 && (int)o.transform.localPosition.z == -6)
            {
                Exit = o;
            }
            else if ((int)o.transform.localPosition.x == 6 && (int)o.transform.localPosition.z == 5)
            {
                Entrance = o;
            }
        }

        
        Destroy(Exit);

    }
}

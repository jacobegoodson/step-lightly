﻿using System;
using UnityEngine;
using System.Collections;

public class EvilTwinAnimationScript : MonoBehaviour
{
    private GameObject player;

    enum AnimationState
    {
        WalkTowards,
        WalkAway
    }
    public AnimationClip Toward;
    private Animation Away;
    public Animator anim;
    private float animationSpeed = 1.2f;
    private float animTime = 0.0f; //Tells us where we are in the animation's timeline.
    private bool anim_Play = true; //Should the animation play?
    private AnimationState CurrentAnimationState = AnimationState.WalkTowards;
    private Camera MainCamera;
    // Use this for initialization
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        MainCamera = GameObject.FindGameObjectWithTag("Player").GetComponent<Camera>();
	    anim = GetComponent<Animator>();
        StartCoroutine(WalkingAnimation());
	    StartCoroutine(LoopAnimation());
	}

    void Update()
    {
        transform.LookAt(transform.position + MainCamera.transform.rotation * Vector3.forward,
        MainCamera.transform.rotation * Vector3.up);
        SetFacingDirection();
    }

    IEnumerator WalkingAnimation()
    {
        while (true)
        {
            while (true)
            {
                animTime += animationSpeed * Time.deltaTime;
                if (animTime > 0.85f)
                {
                    animTime = 0.85f; 
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
            while (true)
            {
                animTime -= animationSpeed * Time.deltaTime;
                if (animTime < 0.15f)
                {
                    animTime = 0.15f;
                    break;
                }
                yield return new WaitForEndOfFrame();
            }
        }
           
    }

    IEnumerator LoopAnimation()
    {
        while (true)
        {
            anim.Play(GetAnimationState(), 0, animTime); //Step through animation manually
            yield return new WaitForEndOfFrame();
        }   
    }

    String GetAnimationState()
    {
        if ((int)CurrentAnimationState == 0)
        {
            return "WalkTowards";
        } 
        else
        {
            return "WalkAwayFrom";
        }
    }

    void SetAnimationState(AnimationState state)
    {
        CurrentAnimationState = state;
    }

    void SetFacingDirection()
    {
        Vector3 PlayerFacing = player.transform.forward;
        Vector3 EnemyFacing =  transform.parent.transform.forward;
        float facingSameDirection = Vector3.Dot(PlayerFacing, EnemyFacing);

        if (facingSameDirection >= 0)
        {
            SetAnimationState(AnimationState.WalkAway);
            return;    
        }
        SetAnimationState(AnimationState.WalkTowards);
    }
}

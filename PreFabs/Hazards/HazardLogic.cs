﻿using UnityEngine;
using System.Collections;

public class HazardLogic : MonoBehaviour {

    public enum Hazard
    {
        Water,Glass
    }

    public Hazard HazardType;

    void OnTriggerStay(Collider col)
    {
        WalkingSounds billyBob = col.GetComponentInParent<WalkingSounds>();
        if (col.GetComponentInParent<Rigidbody>().velocity.sqrMagnitude > 0)
        {
                EmitSound(col);
        }
        switch (HazardType)
        {
            case Hazard.Glass:
                billyBob.OnGlass = true;
                billyBob.OnWater = false;
                break;
            case Hazard.Water:
                billyBob.OnGlass = false;
                billyBob.OnWater = true;
                break;
        }
    }

    void OnTriggerExit(Collider col)
    {
        WalkingSounds billyBob = col.GetComponentInParent<WalkingSounds>();
        billyBob.OnWater = false;
        billyBob.OnGlass = false;
    }

    void EmitSound(Collider col)
    {
        if (col.transform.tag == "Player")
        {
            EventManager.TriggerEvent("PlayerSteppingOnHazard", (int)HazardType);
        }
        else if (col.GetComponent<CharacterAttributes>()._tag == "Twin1")
        {
            EventManager.TriggerEvent("Twin1SteppingOnHazard", (int)HazardType);
        }
        else if (col.GetComponent<CharacterAttributes>()._tag == "Twin2")
        {
            EventManager.TriggerEvent("Twin2SteppingOnHazard", (int)HazardType);
        }
    }
}
